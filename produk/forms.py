from django import forms
from .models import AddProduk

class FromAdd(forms.Form) :
    penjual = forms.CharField(
        label = '',
        widget=forms.TextInput(attrs={
        # 'cols' : '10',
        # 'rows' : '20',
        'class' : 'form-control',
        'id' : 'penjual',
        'placeholder' : 'Nama Penjual/Perusahaan',
        'type' : 'text',
        'required' : True, 
        'style': 'padding: 5%; margin-top: 5%; margin-bottom: 10%; font-size: 16px;'
    }))

    email = forms.EmailField(
        label = '',
        widget=forms.EmailInput(attrs={
        'class' : 'form-control',
        'id' : 'email',
        'placeholder' : 'Email',
        'type' : 'text',
        'required' : True,
        'style': 'padding: 5%; margin-top: 5%; margin-bottom: 10%; font-size: 16px;'
        
    }))

    nama = forms.CharField(
        label ='',
        widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'nama',
        'placeholder' : 'Nama Barang',
        'type' : 'text',
        'required' : True,
        'style': 'padding: 5%; margin-top: 5%; margin-bottom: 10%; font-size: 16px;'
    }))

    harga = forms.IntegerField(
        label = '',
        widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'harga',
        'placeholder' : 'Harga Barang (contoh : 50000)',
        'type' : 'text',
        'required' : True,
        'style': 'padding: 5%; margin-top: 5%; margin-bottom: 10%; font-size: 16px;'
    }))

    stok = forms.IntegerField(
        label = '',
        widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'stok',
        'placeholder' : 'Stok Barang',
        'type' : 'text',
        'required' : True,
        'style': 'padding: 5%; margin-top: 5%; margin-bottom: 10%; font-size: 16px;'
    }))

    deskripsi = forms.CharField(
        label = '',
        widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'deskripsi',
        'placeholder' : 'Deskripsi Barang',
        'type' : 'text',
        'required' : True,
        'style': 'padding: 5%; margin-top: 5%; margin-bottom: 10%; font-size: 16px;'
    }))

    foto = forms.CharField(
        label = '',
        widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'foto',
        'placeholder' : 'Masukkan link Foto Barang',
        'type' : 'text',
        'required' : True,
        'style': 'padding: 5%; margin-top: 5%; margin-bottom: 5%; font-size: 16px;'
    }))