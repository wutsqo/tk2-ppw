from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from .forms import FromAdd
from .models import AddProduk
import json

# Create your views here.
def listproduk(request):
    if request.method =="POST" and request.POST.get("carinama") !=None:
        search = request.POST.get("carinama")
        produk = AddProduk.objects.filter(nama__contains=search)
        response = {
            'produk' : produk,
        }
        return render(request,'listproduk.html', response)
    else:
        produk = AddProduk.objects.all()
        response = {
            'produk' : produk,
        }
        return render(request,'listproduk.html', response)

    # except:
        # return redirect('/listproduk/addproduct')

def addproduk(request) : 
    responses = {}
    if request.method == 'GET':
        form = FromAdd()
        responses['form'] = form
        return render(request, 'addproduk.html', responses)
        
    else:
        if request.is_ajax():
            if request.user.is_authenticated:
                form = FromAdd(request.POST)
                if form.is_valid():
                    produk = AddProduk()
                    produk.penjual = form.cleaned_data['penjual']
                    produk.email = form.cleaned_data['email']
                    produk.nama = form.cleaned_data['nama']
                    produk.harga = form.cleaned_data['harga']
                    produk.stok = form.cleaned_data['stok']
                    produk.deskripsi = form.cleaned_data['deskripsi']
                    produk.foto = form.cleaned_data['foto']
                    produk.save()
                    return JsonResponse({'success': True})
        return JsonResponse({'success': False})
        # return redirect ("/listproduk/")
        return render(request, "addproduk.html", {"form" : form})
