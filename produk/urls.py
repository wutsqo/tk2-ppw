from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views
# from .views import ajax_posting, addpro

app_name = 'produk'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.listproduk, name='listproduk'),
    path("addproduk/", views.addproduk, name='addproduk'),
    # path('postajax', views.ajax_posting, name='ajax'),# ajax-posting / name = that we will use in ajax url
    # path("detailproduct/<int:detail_id>", views.detailproduk, name='detailproduk')
]
#landing pagenya ke list produk, ngambl dr models yg ada di list produk buat detail sm testi
