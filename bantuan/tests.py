from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .models import ModelBantuan
from .views import addPertanyaan
from .forms import FormBantuan
from .apps import BantuanConfig
import json
# Create your tests here.

class UnitTest(TestCase):
    def setUp(self):
        self.ModelBantuan = ModelBantuan.objects.create(name="vioren", question="apa?", answer="iya")
        self.addPertanyaan = reverse("addPertanyaan")

    def test_url_bantuan_is_exist(self):
        response = Client().post('/bantuan/')
        self.assertEqual(response.status_code, 200)
    
    def test_bantuan_using_bantuan_template(self):
        response = Client().post('/bantuan/')
        self.assertTemplateUsed(response, 'bantuan.html')
    
    def test_POST_add(self):
        response = self.client.post(self.addPertanyaan,{'name': 'vioren', 'question': "apa?", 'answer': "iya"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_url_bantuan_using_addQ_func(self):
        found = resolve('/bantuan/')
        self.assertEqual(found.func, addPertanyaan)

    def test_model_can_create_bantuan(self):
        newQuestion = ModelBantuan.objects.create(name="vioren", question="apa ya?", answer="apa")
        counting_all_question = ModelBantuan.objects.all().count()
        self.assertEqual(counting_all_question, 2)
        question_str = ModelBantuan.objects.get(pk=1)
        self.assertEqual(str(question_str), question_str.question)

    def test_ajax(self):
        dict = {
            "name" : "vioren",
            "question" : "apa?"
        }
        response = Client().post("/bantuan/", json.dumps(dict), content_type="bantuan/json")
        # self.assertEqual(response_get.status_code, 301)

    def test_apps(self):
        self.assertEqual(BantuanConfig.name, 'bantuan')




