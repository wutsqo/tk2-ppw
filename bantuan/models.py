from django.db import models

# Create your models here.
class ModelBantuan(models.Model):
    name = models.CharField(max_length=50)
    question = models.TextField(max_length=500)
    answer = models.TextField(blank=True)

    def __str__(self):
        return self.question