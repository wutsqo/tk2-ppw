from django.shortcuts import render
from django.http import JsonResponse
from .models import ModelBantuan

# Create your views here.
def addPertanyaan(request):
    model = ModelBantuan.objects.all()
    response_data = {}

    if request.POST.get('action') == 'post':
        name = request.POST.get('name')
        question = request.POST.get('question')
        response_data['name'] = name
        response_data['question'] = question

        ModelBantuan.objects.create(
            name = name,
            question = question,
        )
        return JsonResponse(response_data)
    return render(request, 'bantuan.html', {'model':model})   

# def addPertanyaan(request):
#     formBantuan = FormBantuan()
#     if request.method == "POST":
#         formBantuan_input = FormBantuan(request.POST)
#         if formBantuan_input.is_valid():
#             data = formBantuan_input.cleaned_data
#             bantuan_input = ModelBantuan()
#             bantuan_input.name = data['name']
#             bantuan_input.question = data['question']
#             bantuan_input.save()
#             pertanyaan = ModelBantuan.objects.all()
#         return render(request, 'bantuan.html', {'form': formBantuan, 'pertanyaan': pertanyaan, 'status': 'failed'})
#     pertanyaan = ModelBantuan.objects.all()
#     return render(request, 'bantuan.html', {'form': formBantuan, 'pertanyaan': pertanyaan,})
