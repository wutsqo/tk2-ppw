$(document).on('submit', '#post-form',function(e){
    $.ajax({
        type:'POST',
        data:{
            name:$('#name').val(),
            question:$('#question').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            action: 'post'
        },
        success:function(json) {
            document.getElementById("post-form").reset();
            // console.log(json); // log the returned json to the console
            // console.log("success"); // another sanity check
        },
        error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText);
        }
    });
});

$(".accordion_tab").click(function(){
    if ($(this).hasClass("active")){
        $(this).removeClass("active");
    } else {
        $(this).addClass("active");
    }
});

$(document).ready(function(){
    $(".accordion_content").click(function(){
      $(this).css("background-color", "antiquewhite");
    });
});