from django import forms
from .models import DetailPengiriman, PaymentMethod, Courier

class FormPengiriman(forms.Form) :
    nama = forms.CharField(
        label = 'Nama Lengkap',
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Nama Lengkap Anda',
            'type' : 'text',
            'required' : True, 
            'style': 'margin-bottom: 10px;'
    }))

    telpon = forms.CharField(
        label = 'Nomor Telepon',
        widget=forms.NumberInput(attrs={
            'class' : 'form-control',
            'placeholder' : '081234567890',
            'type' : 'number',
            'required' : True,
            'style': 'margin-bottom: 10px;'
        })
    )

    email = forms.EmailField(
        label = 'Email',
        widget=forms.EmailInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'emailanda@gmail.com',
            'type' : 'email',
            'required' : True,
            'style': 'margin-bottom: 10px;'
        
    }))

    metode_pembayaran = forms.ModelChoiceField(
        label= 'Metode Pembayaran',
        queryset=PaymentMethod.objects.all(),
        widget=forms.Select(attrs={
            'class' : 'form-control',
            'required' : True,
            'style': 'margin-bottom: 10px;'
    }))

    kurir = forms.ModelChoiceField(
        label= 'Pilih Kurir',
        queryset=Courier.objects.all(),
        widget=forms.Select(attrs={
            'class' : 'form-control',
            'required' : True,
            'style': 'margin-bottom: 10px;'
    }))
    

    alamat = forms.CharField(
        label = 'Alamat Lengkap',
        widget=forms.Textarea(attrs={
            'class' : 'form-control',
            'placeholder' : 'Alamat Lengkap',
            'type' : 'text',
            'required' : True,
            'style': 'margin-bottom: 10px;'
    }))
