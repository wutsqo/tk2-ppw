from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.db import IntegrityError
from django.views.decorators.cache import never_cache
from produk.models import AddProduk
from .models import (
    ProdukDiKeranjang,
    DetailPengiriman,
    ProdukAkanDikirim,
    PaymentMethod,
    Courier,
    StatusPengiriman,
)
from .forms import FormPengiriman
import datetime

"""
def detail(request):
    context = {
        "hide_nav_2": True,
    }
    return render(request, 'keranjang/detail.html', context)
"""


@never_cache
def keranjang(request):

    context = {
        "hide_nav_2": True,
    }

    if request.user.is_authenticated:

        user = User.objects.get_by_natural_key(request.user.username)
        produk = ProdukDiKeranjang.objects.filter(user=user)
        form = FormPengiriman()

        totalHarga = getTotalHarga(request)

        context['produk'] = produk
        context['form'] = form
        context['totalHarga'] = totalHarga

        return render(request, "keranjang/keranjang.html", context)
    else:
        return render(request, "keranjang/keranjang.html", context)


@login_required
def checkout(request):
    user = User.objects.get_by_natural_key(request.user.username)

    if request.method == "POST":

        if ProdukDiKeranjang.objects.filter(user=user).count() != 0:

            produk_di_keranjang = ProdukDiKeranjang.objects.filter(user=user)
            totalHarga = sum(
                [(x.product.harga * x.jumlah) for x in produk_di_keranjang]
            )
            formCheckout = FormPengiriman(request.POST or None)

            if formCheckout.is_valid():

                detailpengiriman = DetailPengiriman.objects.create(
                    user=user,
                    nama=formCheckout.cleaned_data["nama"],
                    telpon=formCheckout.cleaned_data["telpon"],
                    email=formCheckout.cleaned_data["email"],
                    alamat=formCheckout.cleaned_data["alamat"],
                    total_harga=totalHarga,
                    kurir=Courier.objects.get(name=formCheckout.cleaned_data["kurir"]),
                    metode_pembayaran=PaymentMethod.objects.get(
                        name=formCheckout.cleaned_data["metode_pembayaran"]
                    ),
                    status_pengiriman=StatusPengiriman.objects.all().first(),
                )

                for p in produk_di_keranjang:
                    ProdukAkanDikirim.objects.create(
                        product=p.product,
                        detailpengiriman=detailpengiriman,
                        jumlah=p.jumlah,
                    )

                    produk = AddProduk.objects.get(id=p.product.id)
                    produk.stok -= p.jumlah
                    produk.save()

                produk_di_keranjang.delete()
                response = {
                    "detail": detailpengiriman,
                    "hide_nav_2": True,
                    "hide_footer": True,
                }

                return render(request, "keranjang/detail.html", response)
        else:
            return HttpResponseBadRequest("ProdukDiKeranjang instance not found")
    else:
        return redirect("keranjang:keranjang")


def addToKeranjang(request, detail_id):

    if request.user.is_authenticated:
        try:
            produk = AddProduk.objects.get(id=detail_id)
            user = User.objects.get_by_natural_key(request.user.username)

            if len(ProdukDiKeranjang.objects.filter(product=produk, user=user)) == 0:
                ProdukDiKeranjang.objects.create(
                    product=produk,
                    user=user,
                    jumlah=1,
                )

            return redirect("keranjang:keranjang")
        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest("Produk instance not found")
    else:
        return redirect("keranjang:keranjang")


@login_required
def addPOST(request):
    if request.method == "POST":
        try:
            detail_id = request.POST.get("detail_id")
            produk = AddProduk.objects.get(id=detail_id)
            user = User.objects.get_by_natural_key(request.user.username)

            if len(ProdukDiKeranjang.objects.filter(product=produk, user=user)) == 0:
                ProdukDiKeranjang.objects.create(
                    product=produk,
                    user=user,
                    jumlah=1,
                )

            return redirect("keranjang:keranjang")

        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest("Produk instance not found")

    else:
        return HttpResponseBadRequest("GET method is not allowed")


@login_required
def delPOST(request):
    if request.method == "POST":
        try:
            detail_id = request.POST.get("detail_id")
            produk = AddProduk.objects.get(id=detail_id)
            user = User.objects.get_by_natural_key(request.user.username)

            ProdukDiKeranjang.objects.get(product=produk, user=user).delete()

            data = {
                "message": "successfully deleted from keranjang",
                "isNotEmpty": ProdukDiKeranjang.objects.filter(user=user).count(),
            }

            return JsonResponse(data)

        except ProdukDiKeranjang.DoesNotExist:
            return HttpResponseBadRequest("ProdukDiKeranjang instance not found")

        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest("AddProduk instance not found")

    else:
        return HttpResponseBadRequest("GET method is not allowed")


@login_required
def incPOST(request):
    if request.method == "POST":
        try:
            detail_id = request.POST.get("detail_id")
            produk = AddProduk.objects.get(id=detail_id)
            user = User.objects.get_by_natural_key(request.user.username)

            produk_di_keranjang = ProdukDiKeranjang.objects.get(
                product=produk, user=user
            )

            if produk_di_keranjang.jumlah == produk.stok:
                return HttpResponseBadRequest(
                    "ProdukDiKeranjang.jumlah cannot exceeds stok produk"
                )

            else:
                produk_di_keranjang.jumlah += 1
                produk_di_keranjang.save()

                data = {
                    "qty": produk_di_keranjang.jumlah,
                    "harga": produk_di_keranjang.product.harga,
                    "totalHarga": getTotalHarga(request),
                }

                return JsonResponse(data)

        except ProdukDiKeranjang.DoesNotExist:
            return HttpResponseBadRequest("ProdukDiKeranjang instance not found")

        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest("AddProduk instance not found")

    else:
        return HttpResponseBadRequest("GET method is not allowed")


@login_required
def decPOST(request):
    if request.method == "POST":
        try:
            detail_id = request.POST.get("detail_id")
            produk = AddProduk.objects.get(id=detail_id)
            user = User.objects.get_by_natural_key(request.user.username)

            produk_di_keranjang = ProdukDiKeranjang.objects.get(
                product=produk, user=user
            )

            if produk_di_keranjang.jumlah == 1:
                return HttpResponseBadRequest("ProdukDiKeranjang.jumlah cannot be zero")

            else:
                produk_di_keranjang.jumlah -= 1
                produk_di_keranjang.save()

                data = {
                    "qty": produk_di_keranjang.jumlah,
                    "harga": produk_di_keranjang.product.harga,
                    "totalHarga": getTotalHarga(request),
                }

                return JsonResponse(data)

        except ProdukDiKeranjang.DoesNotExist:
            return HttpResponseBadRequest("ProdukDiKeranjang instance not found")

        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest("AddProduk instance not found")

    else:
        return HttpResponseBadRequest("GET method is not allowed")


def getTotalHarga(request):
    user = User.objects.get_by_natural_key(request.user.username)
    produk = ProdukDiKeranjang.objects.filter(user=user)
    totalHarga = sum([(x.product.harga * x.jumlah) for x in produk])

    return totalHarga
