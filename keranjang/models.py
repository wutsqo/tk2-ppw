from django.db import models
from django.contrib.auth.models import User
from produk.models import AddProduk

class ProdukDiKeranjang(models.Model):
    product = models.ForeignKey(AddProduk, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='produkdikeranjang')
    jumlah = models.PositiveIntegerField()
    
    def __str__(self):
        return f'{self.user} : {self.product.nama}'

class PaymentMethod(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Courier(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class StatusPengiriman(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class DetailPengiriman(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    nama = models.CharField(max_length=30)
    telpon = models.CharField(max_length=15)
    email = models.EmailField()
    alamat = models.CharField(max_length=100)
    total_harga = models.PositiveIntegerField()
    waktu_checkout = models.DateTimeField(auto_now_add=True)
    kurir = models.ForeignKey(Courier, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    metode_pembayaran = models.ForeignKey(PaymentMethod, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    status_pengiriman = models.ForeignKey(StatusPengiriman, on_delete=models.SET_NULL, null=True, blank=True, default=None)

    def __str__(self):
        return f'{self.user} : {self.nama} : {self.waktu_checkout}'

class ProdukAkanDikirim(models.Model):
    product = models.ForeignKey(AddProduk, on_delete=models.RESTRICT, related_name='produkakandikirim')
    detailpengiriman = models.ForeignKey(DetailPengiriman, on_delete=models.CASCADE, related_name='produkakandikirim')
    jumlah = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.detailpengiriman.user} : {self.detailpengiriman.nama} : {self.detailpengiriman.waktu_checkout}'