from django.contrib import admin
from django.urls import reverse
from .models import DetailPengiriman, ProdukAkanDikirim, PaymentMethod, Courier, StatusPengiriman, ProdukDiKeranjang
from produk.models import AddProduk
from django.utils.html import format_html

def linkify(field_name):
    """
    Converts a foreign key value into clickable links.
    
    If field_name is 'parent', link text will be str(obj.parent)
    Link will be admin url for the admin url for obj.parent.id:change
    """
    def _linkify(obj):
        linked_obj = getattr(obj, field_name)
        if linked_obj is None:
            return '-'
        app_label = linked_obj._meta.app_label
        model_name = linked_obj._meta.model_name
        view_name = f'admin:{app_label}_{model_name}_change'
        link_url = reverse(view_name, args=[linked_obj.pk])
        return format_html('<a href="{}">{}</a>', link_url, linked_obj)

    _linkify.short_description = field_name  # Sets column name
    return _linkify

@admin.register(ProdukAkanDikirim)
class ProdukAkanDikirimAdmin(admin.ModelAdmin):
    list_display = [
        linkify(field_name="detailpengiriman"),
        linkify(field_name="product"),
    ]

class ProdukAkanDikirimInline(admin.TabularInline):
    model = ProdukAkanDikirim
    readonly_fields = ('product', 'jumlah')
    can_delete = False

@admin.register(DetailPengiriman)
class DetailPengirimanAdmin(admin.ModelAdmin):
    list_display = ('waktu_checkout', 'nama')
    inlines = [ProdukAkanDikirimInline]

admin.site.register(PaymentMethod)
admin.site.register(StatusPengiriman)
admin.site.register(Courier)
admin.site.register(ProdukDiKeranjang)