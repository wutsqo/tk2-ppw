from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .forms import FormTesti
from .models import Testimoni
from produk.models import AddProduk
from keranjang.models import DetailPengiriman, ProdukAkanDikirim
import json
from django.core import serializers


# Create your views here.    
def detailproduk(request, detail_id) :
    produk_detail = AddProduk.objects.get(id=detail_id)
    form_testi = FormTesti(request.POST or None)
    response_data = {}
    if request.method == 'POST':
        if form_testi.is_valid():
            carinama = form_testi.cleaned_data['name']
            produkterpesan = ProdukAkanDikirim.objects.filter(product=produk_detail)
            namapemesan = ProdukAkanDikirim.objects.filter(product=produk_detail).values_list('detailpengiriman__nama',flat=True)
            tagnama = False
            for p in namapemesan:
                if p == carinama:
                    tagnama = True
            if produkterpesan != None and tagnama==True:
                Testimoni.objects.create(
                    name = form_testi.cleaned_data['name'],
                    komentar = form_testi.cleaned_data['komentar'],
                    produk =  produk_detail
                )
                response_data['name'] = form_testi.cleaned_data['name']
                response_data['komentar'] = form_testi.cleaned_data['komentar']
                form_testi = FormTesti()
                # return HttpResponseRedirect(request.path)
                return JsonResponse(response_data)
            else:
                # testimoni = Testimoni.objects.all()
                # produks = AddProduk.objects.all()
                # context = {
                #     'page_title':'Testimoni',
                #     'form_testi':form_testi,
                #     'pd':produk_detail,
                #     'testimoni':testimoni,
                #     'produks':produks,
                #     'notfound':'Nama tidak ditemukan, pastikan Anda sudah memesan barang dan menginput nama yang sama dengan nama saat Anda memesan'
                # }
                response_data['name'] = "null"
                return JsonResponse(response_data)
                # return render(request,'detailproduk.html',context)
    
    testimoni = Testimoni.objects.all()
    produks = AddProduk.objects.all()
    context = {
        'page_title':'Testimoni',
        'form_testi':form_testi,
        'pd':produk_detail,
        'testimoni':testimoni,
        'produks':produks
    }
    return render(request,'detailproduk.html',context)

# blm kepake
# def testijson(request, detail_id):
#     produk_detail = AddProduk.objects.get(id=detail_id)
#     testimoni = Testimoni.objects.filter(produk=produk_detail)
#     return JsonResponse(testimoni)
        # testihasil = serializers.serialize('json',testimoni)
        # datatesti = json.loads(testihasil) #string to json
        # # ret = json.dumps(datatesti) #json to string
        # return JsonResponse(datatesti, content_type="application/json")
    