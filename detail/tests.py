# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve,reverse
from django.apps import apps
from produk.models import AddProduk
from .models import Testimoni
from .views import detailproduk
from .forms import FormTesti
from .apps import DetailConfig
from django.contrib.auth.models import User

class TestModel(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="halo", password="halohalobandung")
        self.produk = AddProduk.objects.create(nama="A",harga="20000",penjual="B",email="y@gmail.com",deskripsi="Barang")
        self.testi = Testimoni.objects.create(name="Yona", komentar="lulus PPW yuuk",produk=self.produk)

    def test_model_user_created(self):
        self.assertEqual(User.objects.all().count(), 1)

    def test_model_testi_created(self):
        self.assertEqual(Testimoni.objects.all().count(), 1)

    def test_model_testi_fields(self):
        self.assertEqual(str(self.testi), "Yona")

class TestUrls(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(nama="A",harga="20000",penjual="B",email="y@gmail.com",deskripsi="Barang")
        self.testi = Testimoni.objects.create(name="Yona", komentar="lulus PPW yuuk",produk=AddProduk.objects.get(nama="A"))
        self.detailproduct = reverse('detail:detailproduk', args=[self.produk.id])

    def test_urls_detail_test(self):
        found = resolve(self.detailproduct)
        self.assertEqual(found.func, detailproduk)

class TestViews(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(nama="A",harga="20000",penjual="B",email="y@gmail.com",deskripsi="Barang")
        self.detailproduct = reverse('detail:detailproduk', args=[self.produk.id])

    def test_detailproduct_views(self):
        response = Client().get(self.detailproduct)
        self.assertEqual(response.status_code, 200) 
        self.assertTemplateUsed(response, "detailproduk.html")

class TestForm(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(nama="A",harga="20000",penjual="B",email="y@gmail.com",deskripsi="Barang")

    def test_addtestimoni_get(self):
        response = Client().get("/detail/1")
        self.assertEqual(response.status_code, 200) 
        self.assertTemplateUsed(response, "detailproduk.html")

    def test_addtestimoni_post(self):
        response = Client().post("/detail/1", data={"name":"S", "komentar":"PPW keren"}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_addtestimoni_invalidpost(self):
        response = Client().post("/detail/1", data={"name":"", "komentar":""}, follow=True)
        self.assertTemplateUsed(response, "detailproduk.html")

class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(DetailConfig.name, 'detail')

