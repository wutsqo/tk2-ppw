from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'detail'

urlpatterns = [
    path('admin/', admin.site.urls),
    path("<int:detail_id>", views.detailproduk, name='detailproduk'),
    # path("testijson", views.testijson, name='testijson')
]