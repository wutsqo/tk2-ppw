from django import forms
from .models import Testimoni

class FormTesti(forms.Form):
    class Meta:
        model = Testimoni
        fields = ['name', 'komentar']
        widgets ={
            'name' : forms.TextInput(attrs={'class': 'form-control'}),
            'komentar' : forms.Textarea(attrs={'class': 'form-control'}),
        }

    error_messages = {
        'required' : 'Please input text'
    }

    name = forms.CharField(
        label='', required=True,
        max_length=50,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Nama',
            'id' : 'testiname'}))

    komentar = forms.CharField(
        label='', required=True,
        max_length=200,
        widget=forms.Textarea(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Ulasan',
            'id' : 'testikomentar'}))