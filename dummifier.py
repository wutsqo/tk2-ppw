import csv

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','tkppw.settings')

import django
django.setup()

from django.core.management import call_command

from produk.models import AddProduk
with open('MOCK_DATA.csv') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=';')
	line_count = 1
	for row in csv_reader:
		produk = AddProduk.objects.create(
			penjual = row[0],
			email = row[1],
			nama = row[2],
			harga = int(row[3]),
			stok = int(row[4]),
			deskripsi = row[5],
			foto = row[6]
		)
		print(line_count)
		line_count += 1