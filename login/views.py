from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import request
from django.shortcuts import render, redirect, reverse
from keranjang.models import DetailPengiriman
from produk.models import AddProduk
from .forms import RegisterForm, LoginForm
from .models import User


def signUp(request):
    formSignUp = RegisterForm()
    if request.method == "POST":
        formSignUp_input = RegisterForm(request.POST)
        if formSignUp_input.is_valid():
            username = request.POST["username"]
            fullname = request.POST["fullname"]
            phone = request.POST["phone"]
            email = request.POST["email"]
            password = request.POST["password1"]
            if User.objects.filter(username=username).exists():
                messages.error(request, "username is already taken!")
            else:
                user = User.objects.create_user(username, email, password)
                return redirect("/account/login")
    return render(request, "signup.html", {"form": formSignUp})


def log_in(request):
    formLogin = LoginForm()
    if request.method == "POST":
        formLogin_input = LoginForm(request.POST)
        if formLogin_input.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            if User.objects.filter(username=username).exists():
                user = authenticate(request, username=username, password=password)
                if user is not None:
                    login(request, user)
                    return redirect("/account/")
                else:
                    messages.error(
                        request, "your password is invalid, please try again"
                    )
            else:
                messages.error(request, "your username is invalid, please try again")
    return render(request, "login.html", {"form": formLogin})


def log_out(request):
    logout(request)
    return redirect("/")


@login_required
def dashboard(request):
    context = {
        "hide_nav_2": True,
    }
    return render(request, "dashboard.html", context)


@login_required
def history(request):
    context = {
        "hide_nav_2": True,
    }

    user = User.objects.get_by_natural_key(request.user.username)
    details = DetailPengiriman.objects.filter(user=user)

    context["details"] = details

    return render(request, "keranjang/history.html", context)


@login_required
def history_detail(request, detail_id):

    context = {
        "hide_nav_2": True,
    }
    
    try:

        user = User.objects.get_by_natural_key(request.user.username)
        detail = DetailPengiriman.objects.filter(user=user)[detail_id - 1]

        context["detail"] = detail

        return render(request, "keranjang/detail.html", context)
    except IndexError:
        return redirect('/account/history/')

# @login_required
# def history_produk(request):
#     context = {
#         "hide_nav_2": True,
#     }

#     user = User.objects.get_by_natural_key(request.user.username)
#     details = AddProduk.objects.filter(user=user)

#     context["details"] = details

#     return render(request, "produk/riwayatproduk.html", context)