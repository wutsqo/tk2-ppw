from django.http import response
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from django.apps import apps
from keranjang.models import DetailPengiriman
from .models import Profile
from .apps import LoginConfig
from .forms import LoginForm, RegisterForm
from .views import signUp, log_in, log_out

class UnitTest(TestCase):
    def setUp(self):
        self.userData = {
            "username": "hahaha",
            'fullname' : 'full name',
            'phone' : '012345678901',
            "email": "hahaha@gmail.com",
            'password1': '345345345a',
            'password2': '345345345a',
        }
        self.user = User.objects.create_user('hahaha', "hahaha@gmail.com", '345345345a')

    def test_homepage_url_exists(self):
        response = Client().get('/account/')
        self.assertEqual(response.status_code, 302)

    def test_signup_url_exists(self):
        response = Client().get('/account/signup/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_exists(self):
        response = Client().get('/account/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exists(self):
        response = Client().get('/account/logout/')
        self.assertEqual(response.status_code, 302)

    def test_create_model(self):
        User.objects.create_user(
            username="vioren", email="vioren@gmail.com", password="vvdrn")
        self.assertEqual(User.objects.all().count(), 2)

    def test_apps(self):
        self.assertEqual(LoginConfig.name, 'login')
    
    def test_forms_register_valid(self):
        form_reg = RegisterForm(data={
            "username": "hahaha",
            'fullname' : 'full name',
            'phone' : '012345678901',
            "email": "hahaha@gmail.com",
            'password1': '345345345a',
            'password2': '345345345a',
        })
        self.assertTrue(form_reg.is_valid())

    def test_forms_register_invalid(self):
        form_regs = RegisterForm(data={
            "username": "",
            "email": "hahaha@gmail.com",
            'password1': '345345345',
            'password2': '345345345',
        })
        self.assertFalse(form_regs.is_valid())

    def test_forms_login_valid(self):
        form_login = LoginForm(data={
            "username": "haihai",
            "password": "hihihihi"
        })
        self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = LoginForm(data={
            "username": "",
            "password": ""
        })
        self.assertFalse(form_login.is_valid())

    def test_user_login(self):
        response = self.client.post('/login', data=self.userData, follow=True)
        self.assertFalse(response.context['user'].is_authenticated)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login = reverse("login")
        self.regs = reverse("signup")

    def test_POST_reg_valid(self):
        response = self.client.post(self.regs,{
            "username": "hahaha",
            'fullname' : 'full name',
            'phone' : '012345678901',
            "email": "hahaha@gmail.com",
            'password1': '345345345a',
            'password2': '345345345a',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_reg_invalid(self):
        response = self.client.post(self.regs,{
            "username": "hahaha",
            "email": "hahaha@gmail.com",
            'password1': 'hahahaha',
            'password2': '',
        }, follow=True)
        self.assertTemplateUsed(response, 'signup.html')

    def test_POST_login_valid(self):
        response = self.client.post(self.login,
        {
            'username': "hahaha",
            'password ': "hahahaha"
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_login_invalid(self):
        response = self.client.post(self.login,
        {
            'username': "",
            'password ': ""
        }, follow=True)
        self.assertTemplateUsed(response, 'login.html')

    def test_GET_dashboard(self):
        User.objects.create_user(username='testuser', password='Mickey4Mouse')
        response = self.client.login(username='testuser', password='Mickey4Mouse')
        response = self.client.get('/account/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dashboard.html')

    def test_GET_history(self):
        User.objects.create_user(username='testuser', password='Mickey4Mouse')
        response = self.client.login(username='testuser', password='Mickey4Mouse')
        response = self.client.get('/account/history/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'keranjang/history.html')

    def test_GET_history_detail(self):
        User.objects.create_user(username='testuser', password='Mickey4Mouse')
        response = self.client.login(username='testuser', password='Mickey4Mouse')
        response = self.client.get('/account/history/1', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'keranjang/history.html')