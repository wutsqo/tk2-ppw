from django.urls import path
from .views import dashboard, history, history_detail, signUp, log_in, log_out
from .models import User

urlpatterns = [
    path('signup/', signUp, name='signup'),
    path('login/', log_in, name='login'),
    path('logout/', log_out, name='logout'),
    path('', dashboard, name='dashboard'),
    path('history/', history, name='riwayat'),
    path('history/<int:detail_id>', history_detail, name='riwayat_detail'),
    # path('historyproduk/', history_produk , name='riwayatproduk'),
]
