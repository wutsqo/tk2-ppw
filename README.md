
[![pipeline status](https://gitlab.com/wutsqo/tk2-ppw/badges/master/pipeline.svg)](https://gitlab.com/wutsqo/tk2-ppw/-/commits/master) [![coverage report](https://gitlab.com/wutsqo/tk2-ppw/badges/master/coverage.svg)](https://gitlab.com/wutsqo/tk2-ppw/-/commits/master)

# Tugas Kelompok 2 PPW
Kelas: PPW-D
Kelompok: D07

## Anggota:
1. Herbiyona
2. Vioren Paramitta Adithana
3. Muhammad Urwatil Wutsqo
4. Siti Nurlaila

## Link Herokuapp:
https://tk2ppw.herokuapp.com
