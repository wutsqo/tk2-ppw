$(document).ready(function(){
  $("#testipost").submit(function(event) {
    event.preventDefault();
    let hasilred = window.location.pathname
    $.ajax({
      url: hasilred,
      type:"POST",
      dataType:'json',
      data :{
        name:$("#testiname").val(),
        komentar:$("#testikomentar").val(),
        csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
      },
      success : function(json){
        if (json.name != "null"){
          document.getElementById("testipost").reset();
          $('#testiHasilContainer').append(`
          <div class="flex-container testihasil" style="width: 100%; display: flex; overflow-wrap: break-word;">
                <!-- disini pake ajax -->
                <div style="width: 100%; display: flex; text-align: left;"> 
                    <div class="container detail" style="align-self: center; width: 100%;margin-top: 2%;">
                        <h4 style="line-height: 170%;">
                            ${json.komentar}<br>
                        </h4>
                        <h5>
                            -${json.name} 
                        </h5> 
                    </div>
                </div>
            </div>
          `);
        }
        else{
          alert("Testimoni tidak berhasil ditambahkan. Pastikan Anda sudah memesan barang dan menginput nama yang benar!")
        }
      }
    })
  });

  $(".detail").mouseenter(function(){
    $(this).css({"background-color": "lightgrey"});
  });
  $(".detail").mouseleave(function(){
    $(this).css({"background-color": "white"});
  });

  $(".buttonbeli").mouseenter(function(){
    $(this).css({"background-color": "white", "color":"#00395E", "border":"1px solid blue"});
  });
  $(".buttonbeli").mouseleave(function(){
    $(this).css({"background-color":"#00395E", "border": "#00395E", "color":"white"});
  });
});