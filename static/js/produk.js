$(document).ready(function(){

    $("#submit").click(function(e){
        e.preventDefault();
        submitForm();
    })

})

function submitForm(){
    $.ajax({
        type: "POST",
        data: $("form#formpd").serialize(),

        error: function(){
            alert("Something went wrong, try again.")
        },
        success: function(data){
            console.log(data)
            // $('#output').html("sukses") /* response message */ 
            if (data.success == true) {
                window.setTimeout(function() {
                    alert("Produk berhasil ditambahkan!");
                    location.href='/listproduk';
                }, 200);
            } else if (data.success == false) {
                alert("Produk gagal ditambahkan")
            }
        },
        
    })
}

var btn = $('#buttonproduk');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

// $("#listpd").each(function(){
//     $("#listpd").mouseover(function() {
//         $("#listpd").css("background-color", "#00395E");
//         $("#detailbt").css("background-color", "white");
//         $("#detailbt").css("color", "#00395E");
//         $("#listpd").css("color", "white");
//     });
// });
  
// $("#listpd").mouseout(function(){
//     $("#listpd").css("background-color", "white");
//     $("#detailbt").css("background-color", "#00395E");
//     $("#detailbt").css("color", "white");
//     $("#listpd").css("color", "black");
//     });

$("#dongkerbt").mouseover(function() {
    $("#dongkerbt").css("background-color", "white");
});
$("#dongkerbt").mouseout(function() {
    $("#dongkerbt").css("background-color", "#00395E");
});

$("#dongkerbt").click(function() {
    $("#cardadd").css("background-color", "#00395E");
});


$("#birubt").mouseover(function() {
    $("#birubt").css("background-color", "white");
});
$("#birubt").mouseout(function() {
    $("#birubt").css("background-color", "#CEE8F9");
});

$("#birubt").click(function() {
    $("#cardadd").css("background-color", "#CEE8F9");
});


$("#coklatbt").mouseover(function() {
    $("#coklatbt").css("background-color", "white");
});
$("#coklatbt").mouseout(function() {
    $("#coklatbt").css("background-color", "#E0C387");
});

$("#coklatbt").click(function() {
    $("#cardadd").css("background-color", "#E0C387");
});

