$(".delItem").click(function () {
  console.log("del");
  var detail_id = $(this)
    .parent()
    .parent()
    .parent()
    .parent()
    .parent()
    .attr("id");
    var csrftoken = Cookies.get("csrftoken");
    $.ajax({
      type: "POST",
      url: "/keranjang/delPOST",
      headers: { "X-CSRFToken": csrftoken },
      data: {
        detail_id: detail_id,
      },
      success: function (data) {
        console.log(data.message)
        $(".item"+detail_id).remove();
        if (!data.isNotEmpty) {
          location.reload();
        }
      },
    });        
});
$(".incQty").click(function () {
  var detail_id = $(this)
    .parent()
    .parent()
    .parent()
    .parent()
    .parent()
    .attr("id");
  console.log("inc");
  console.log(detail_id);
  var csrftoken = Cookies.get("csrftoken");
  console.log(csrftoken);
  $.ajax({
    type: "POST",
    url: "/keranjang/incPOST",
    headers: { "X-CSRFToken": csrftoken },
    data: {
      detail_id: detail_id,
    },
    success: function (data) {
      draw(detail_id, data);
    },
  });
});
$(".decQty").click(function () {
  console.log("dec");
  var detail_id = $(this)
    .parent()
    .parent()
    .parent()
    .parent()
    .parent()
    .attr("id");
  var csrftoken = Cookies.get("csrftoken");
  $.ajax({
    type: "POST",
    url: "/keranjang/decPOST",
    headers: { "X-CSRFToken": csrftoken },
    data: {
      detail_id: detail_id,
    },
    success: function (data) {
      draw(detail_id, data);
    },
  });
});
function draw(detail_id, data) {
  $(".qty" + detail_id).html(data.qty);
  $(".price" + detail_id).html(thousep(data.qty * data.harga));
  $("#totalPrice").html(thousep(data.totalHarga));
}
function thousep(n) {
  if (typeof n === "number") {
    n += "";
    var x = n.split(".");
    var x1 = x[0];
    var x2 = x.length > 1 ? "." + x[1] : "";
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, "$1" + "." + "$2");
    }
    return x1 + x2;
  } else {
    return n;
  }
}
